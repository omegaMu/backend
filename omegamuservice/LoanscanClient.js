process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';

const axios = require('axios')

const headers = {
    'Content-Type': 'application/json',
    'rejectUnauthorized': false,
    'strictSSL': false
};

const negativeTickStatuses = [
    'default',
    'delinquent'
]

const positiveStatuses = [
    'paid_off'
]

class LoanscanClient {

    constructor() {
        this.exchangeRates = []
    }

    async getLoansDetailsScore(loans, rates) {
        // console.log('all the loans for this person', loans)
        let totalLoanDetailsScore = {}
        let score = 0

        this.exchangeRates = rates

        let loansDetails = await this.getAllLoansDetails(loans)
        // console.log('details', loansDetails)

        let totalDebtScore = await this.getTotalDebtScore(loansDetails)
        // console.log('total debt score', totalDebtScore)
        score += totalDebtScore.score

        let totalNegativeTicksScore = await this.getNegativeTickScore(loansDetails)
        console.log('totalNegativeTicksScore', totalNegativeTicksScore)


        let positiveFactor = await this.getPositiveFactor(loansDetails)
        console.log('factor score', positiveFactor)
        totalNegativeTicksScore.score = Math.round(positiveFactor.factor * totalNegativeTicksScore.score * 2) / 2 // rounds to nearest 1/2

        console.log('totalNegativeTicksScore2', totalNegativeTicksScore)

        score += totalNegativeTicksScore.score

        totalLoanDetailsScore = {
            score,
            totalDebtScore,
            totalNegativeTicksScore,
            positiveFactor
        }

        return totalLoanDetailsScore
    }

    getPositiveFactor(loansDetails) {
        const oneYearAgo = new Date().setFullYear(new Date().getFullYear() - 1)
        let repaidLoansInLastYear = 0
        let factor = .2

        loansDetails.forEach(loan => {
            const loanDate = new Date(loan.maturityDate).getTime()
            // console.log('loan date', loanDate)
            if (positiveStatuses.includes(loan.status) && loanDate >= oneYearAgo) repaidLoansInLastYear += 1
        })

        if (repaidLoansInLastYear === 1) factor = .3
        if (repaidLoansInLastYear >= 2 && repaidLoansInLastYear <= 5) factor = .5
        if (repaidLoansInLastYear > 5 && repaidLoansInLastYear <= 10) factor = 1
        if (repaidLoansInLastYear > 10) factor = .75

        return {
            repaidLoansInLastYear,
            factor
        }
    }

    getAllLoansDetails(loans) {

        let loanPromises = []
        // console.log('promises')
        loans.forEach(loan => {
            loanPromises.push(this.getLoanDetails(loan))
        })

        //promise.all easy way to group large amount of async calls
        return Promise.all(loanPromises).then(data => {
            // console.log('datatatata', data.filter(datum => datum !== null))
            return new Promise((resolve, reject) => {
                resolve(data.filter(datum => datum !== null))
            })
        })
    }

    getLoanDetails(loan) {
        return new Promise((resolve, reject) => {
            axios.get(`https://loanscan.io/api/loans/${loan}`)
                .then(loanDetails => {
                    // console.log('DETAILS', loanDetails.data)
                    if (loanDetails.data) {
                        resolve(loanDetails.data)
                    } else resolve(null)
                })
                .catch(err => {
                    // console.log('err', err.status, loan)
                    resolve(null)
                })
        })
    }

    getNegativeTickScore(loansDetails) {
        let negativeTicks = 0
        let score = 0
        let monthsSinceMostRecentTick = 60
        // console.log('negative ticks')
        loansDetails.forEach(loanDetails => {
            if (negativeTickStatuses.includes(loanDetails.status)) {
                console.log(loanDetails.time, loanDetails.status)
                let today = new Date()
                let delinquentDate = new Date(loanDetails.time)
                //uncomment to test different date ranges. current loans are all very short, not really able to test all values in range
                // let delinquentDate = new Date('Jan 5 2010')
                // let delinquentDate = new Date('Jan 5 2017')
                let months = today.getMonth() - delinquentDate.getMonth() + (12 * (today.getFullYear() - delinquentDate.getFullYear()));

                if (today.getDate() < delinquentDate.getDate()) {
                    months--;
                }

                if (months < monthsSinceMostRecentTick) monthsSinceMostRecentTick = months
                negativeTicks += 1
            }
        })

        if (monthsSinceMostRecentTick < 6) score = 10
        if (monthsSinceMostRecentTick >= 6 && monthsSinceMostRecentTick < 12) score = 20
        if (monthsSinceMostRecentTick >= 12 && monthsSinceMostRecentTick < 24) score = 25
        if (monthsSinceMostRecentTick >= 24 && monthsSinceMostRecentTick < 59) score = 30
        if (monthsSinceMostRecentTick >= 60 || negativeTicks === 0) score = 40

        // console.log('neg tick score', score)
        return {
            monthsSinceMostRecentTick,
            score
        }
    }

    getTotalDebtScore(loansDetails) {
        let loanDebtPromises = []
        loansDetails.forEach(loanDetails => {
            if (loanDetails.status !== "paid_off") {
                loanDebtPromises.push(this.getLoanOpenDebt(loanDetails))
            }
        })

        //because we have to convert the currency, we have to set up another promise.all, because we have to wait on an api to get the rates
        return Promise.all(loanDebtPromises).then(data => {
            return new Promise((resolve, reject) => {
                const totalDebt = data.length ? data.reduce((prev, next) => prev + next) : 0
                let score = 0

                if (totalDebt < 1000) score = 20
                if (totalDebt >= 1000 && totalDebt < 5000) score = 15
                if (totalDebt >= 5000 && totalDebt < 10000) score = 10
                if (totalDebt >= 10000 && totalDebt < 100000) score = 20
                if (totalDebt >= 100000) score = 0

                resolve({
                    score,
                    totalDebt
                })
            })
        })
    }

    getLoanOpenDebt(loanDetails) {
        return new Promise((resolve, reject) => {
            let total, repaid
            total = loanDetails.totalAmount
            repaid = loanDetails.cumulativeRepaid
            // console.log('total repaid', total, repaid)
            // console.log(loanDetails)
            if (!loanDetails) resolve(0);

            // const rate = this.getConversionRate(loanDetails.tokenSymbol)
            // console.log('rate', rate)
            this.getConversionRate(loanDetails.tokenSymbol)
                .then(rate => {
                    resolve(rate * (total - repaid))
                })
                .catch(err => {
                    resolve(0)
                    console.log('er at open debtr', err)
                })
        })
    }

    // getUnknownConversionRate(symbol) {
    //     return new Promise((resolve, reject) => {

    //     })
    // }

    getConversionRate(symbol) {
        return new Promise((resolve, reject) => {
            if (this.exchangeRates[symbol]) resolve(this.exchangeRates[symbol])

            else {
                axios.get(`https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?symbol=${symbol}&convert=DAI`, {
                    headers: {
                        'X-CMC_PRO_API_KEY': '4febe3cf-b488-4dd5-af0c-685812021079'
                    }
                })
                    .then(res => {
                        resolve(res.data.data[symbol].quote.DAI.price)
                    })
                    .catch(err => {
                        console.log('conversion error', symbol)
                        resolve(0)

                    })
            }
        })
    }
}

module.exports = LoanscanClient;
