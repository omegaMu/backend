const ApolloClient = require("apollo-boost").default;
const gql = require("graphql-tag");

const client = new ApolloClient({
    uri: "https://graphql.makerdao.com/v1"
});

class CreditMixClient {
    async getCreditMix(ethAddress, hasLoans) {
        console.log('eth address', ethAddress)
        let types = 0
        let score = 0

        const CDPs = await this.getCDPs(ethAddress)

        if (hasLoans > 0) types += 1
        if (CDPs > 0) types += 1

        if (types === 1 || types === 2) score = .5
        if (types >= 3 && types <= 5) score = 1.5
        if (types >= 6 && types <= 10) score = 2.5
        if (types > 10) score = 5

        return {
            types,
            score
        }
    }

    async getCDPs(ethAddress) {
        return new Promise((resolve, reject) => {
            client
                .query({
                    query: gql`
                {
                    allCups(filter: { lad:{equalTo: "${ethAddress}" } }) {nodes {
                        id
                        lad
                    }}
                  } `
                })
                .then(result => {
                    console.log('result', JSON.stringify(result.data.allCups.nodes.length))
                    resolve(result.data.allCups.nodes.length)
                })
                .catch(err => {
                    console.log('error ql', err)
                    resolve(0)
                })
        })

    }
}

module.exports = CreditMixClient