// Added as hack to overcome TLS cert error 'Error: unable to verify the first certificate'  
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
//const fetch = require("node-fetch");

const axios = require('axios')


const esHost = 'https://api.etherscan.io/';
const apiPath = 'api?';
const accountBalancePath = apiPath + 'module=account&action=balance&address=';
const accountTxPath = apiPath + 'module=account&action=txlist&address='
const numTrans = '&startblock=0&endblock=99999999&page=1&offset=500&sort=asc&apikey='

const apiTag = '&tag=latest&apikey='
const apiKey = 'IDVBPFEE5E3HPHKP7Y3N16B9YUBRNU33R5'

const headers = {
    'Content-Type': 'application/json',
    'rejectUnauthorized': false,
    'strictSSL': false
};

class EtherscanClient {

    constructor() {
        this.exchangeRates = []
    }

    async getAccountBalanceScore(ethAddress, rates) {
        this.exchangeRates = rates
        // console.log('get accoutn points called', ethAddress)

        const loadRequestUrl = esHost + accountBalancePath + ethAddress + apiTag + apiKey;
        const balance = await this.makeGetRequest(ethAddress)
        // console.log('balance in ether')
        const score = await this.calculatePointsByBalance(balance)
        return {
            score: score.score,
            balance: score.balance
        }
    }

    makeGetRequest(ethAddress) {
        return new Promise((resolve, reject) => {
            axios.get(`${esHost}${accountBalancePath}${ethAddress}${apiTag}${apiKey}`)
                .then(res => {
                    // console.log('rether scan res', res.data.result)
                    resolve(res.data.result / Math.pow(10, 18))
                })
                .catch(err => {
                    resolve(0)
                    // console.log('err', err)
                })
        })
    }

    calculatePointsByBalance(balance) {
        // console.log('running cal')
        return new Promise((resolve, reject) => {
            this.getConversionRate('ETH')
                .then(res => {
                    let balanceScore = 0
                    const balanceInDai = res * balance

                    if (balanceInDai > 100000) balanceScore = 5
                    if (balanceInDai >= 10000 && balanceInDai < 100000) balanceScore = 2.5
                    if (balanceInDai >= 1000 && balanceInDai < 10000) balanceScore = 1.5
                    if (balanceInDai >= 1 && balanceInDai < 1000) balanceScore = .5
                    if (balanceInDai < 1) balanceScore = 0

                    resolve({
                        score: balanceScore,
                        balance: balanceInDai
                    })
                })
                .catch(err => resolve({ score: 0, balance: 0 }))
        })
    }

    getConversionRate(symbol) {
        return new Promise((resolve, reject) => {
            if (this.exchangeRates[symbol]) resolve(this.exchangeRates[symbol])

            else {
                axios.get(`https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?symbol=${symbol}&convert=DAI`, {
                    headers: {
                        'X-CMC_PRO_API_KEY': '4febe3cf-b488-4dd5-af0c-685812021079'
                    }
                })
                    .then(res => {
                        resolve(res.data.data[symbol].quote.DAI.price)
                    })
                    .catch(err => {
                        resolve(0)
                        console.log('conversion error', symbol)
                    })
            }
        })
    }

    async getAccountDetailsScore(ethAddress, rates) {




        let accountDetailsScore = 0

        let accountTransactionDetailsScore = await this.getAccountTransactionDetailsScore(ethAddress)

        // console.log('account transacts', accountTransactionDetailsScore)
        return accountTransactionDetailsScore
    }

    async getAccountTransactionDetailsScore(ethAddress, rates) {
        this.exchangeRates = rates

        let accountDetailsScore = {}

        let accountTransactions = await this.getAccountTransactions(ethAddress)
        accountTransactions = await this.getErc721Statuses(accountTransactions)


        let ethToDai = await this.getConversionRate('ETH')
        let meaningfulActivityScore = await this.getMeaningfulActivityScore(accountTransactions, ethToDai)

        // console.log('mening ful activity score object', meaningfulActivityScore)

        return meaningfulActivityScore
    }

    getAccountTransactions(ethAddress) {
        return new Promise((resolve, reject) => {
            axios.get(`${esHost}${accountTxPath}${ethAddress}${numTrans}${apiKey}`)
                .then(res => {
                    // console.log(esHost + accountTxPath + ethAddress + numTrans + apiKey)
                    // console.log('txs', JSON.stringify(res.data.result))
                    const txs = res.data.result.map(tx => {
                        tx.date = new Date(tx.timeStamp * 1000) //timestamps are in unix, multiply by 1000 to get millis for javascript date
                        return tx
                    })

                    resolve(txs)
                })
        })
    }

    getErc721Statuses(transactions) {
        let transactionPromises = []
        // console.log('ger erc721', transactions)
        transactions.forEach(transaction => {
            transactionPromises.push(this.getErc721Status(transaction))
        })

        return Promise.all(transactionPromises).then(data => {
            // console.log('trans', data)
            return new Promise((resolve, reject) => {
                resolve(data.filter(datum => datum !== null))
            })
        })

    }

    getErc721Status(transaction) {
        return new Promise((resolve, reject) => {
            axios.get(`https://3qlpihpd79.execute-api.us-east-1.amazonaws.com/dev/getErc721Status/${transaction.to}`)
                .then(res => {
                    // console.log('the res', res.data.status, transaction.to)
                    transaction.erc721Status = res.data.status
                    // console.log('res', res.data.status)
                    resolve(transaction)
                })
                .catch(err => {
                    transaction.erc721Status = false
                    resolve(transaction)
                })
        })
    }

    async getMeaningfulActivityScore(transactions, ethToDai) {
        let score = 0
        // console.log(transactions[0])
        const meaningfulTransactionDates = await this.getMeaningfulTransactionDates(transactions, ethToDai)
        // console.log('mening', meaningfulTransactionDates[0])
        //the api we have available (ethplorer) will only return 1 yr of values, 
        //so we can use the length of meaningful transaction dates as the number of meaningful transactions over the last year

        const monthsWithActivityMeaningfulTransactions = await this.getMonths(meaningfulTransactionDates)
        // console.log('monthsWithActivityMeaningfulTransactions', monthsWithActivityMeaningfulTransactions)
        const monthsWithActivityScore = await this.getMonthsWithActivityScore(monthsWithActivityMeaningfulTransactions)
        score += monthsWithActivityScore.score

        const avgDailyActivityScore = await this.getAvgDailyActivityScore(meaningfulTransactionDates.length)
        score += avgDailyActivityScore.score

        let activityScore = {
            score,
            avgDailyActivityScore,
            monthsWithActivityScore
        }

        // console.log('activity score', activityScore, monthsWithActivityScore, avgDailyActivityScore)

        return activityScore

    }

    getMeaningfulTransactionDates(transactions, ethToDai) {

        // console.log('transactions to calc', transactions)
        const meaningfulDates = []
        transactions.forEach(transaction => {
            // console.log(transaction.date)
            // console.log('transactino date', transaction.date)
            if (transaction.erc721Status == true) {
                // console.log('in erc721 status == true', transaction.value * ethToDai)
                if ((transaction.value * ethToDai) >= 5) {
                    // console.log('in greater than 5 erc')
                    // console.log(transaction.date)
                    meaningfulDates.push(transaction.date)
                }
            } else {
                // console.log('not erc721 transact', transaction.value * ethToDai)
                if ((transaction.value * ethToDai) >= 20) {
                    // console.log('in greater than 20 non erc')
                    // console.log(transaction.date)
                    meaningfulDates.push(transaction.date)
                }
            }
        })

        // console.log('why not meaningful', meaningfulDates)
        // console.log('meaningful dates', meaningfulDates)
        return meaningfulDates
    }

    getMonths(dates) {
        // dates = [
        //     '2018-08-19T02:36:39.000Z',
        //     '2018-07-19T02:36:39.000Z',
        //     '2018-06-19T02:36:39.000Z',
        //     '2018-05-19T02:36:39.000Z',
        //     '2018-04-19T02:36:39.000Z',
        //     '2018-03-19T02:36:39.000Z',
        //     '2018-02-19T02:36:39.000Z',
        //     '2018-01-19T02:36:39.000Z',
        //     '2017-12-19T02:36:39.000Z',
        //     '2017-11-19T02:36:39.000Z',
        //     '2017-10-19T02:36:39.000Z',
        //     '2017-09-19T02:36:39.000Z',
        //     '2017-08-19T02:36:39.000Z',
        //     '2017-06-19T02:36:39.000Z',
        //     '2018-04-19T02:36:39.000Z',
        //     '2018-03-19T02:36:39.000Z',
        //     '2018-02-19T02:36:39.000Z',
        //     '2018-01-19T02:36:39.000Z',
        //     '2017-10-19T02:36:39.000Z',
        //     '2017-04-19T02:36:39.000Z',
        // ]

        const monthsWithActivity = []
        // console.log('dates', dates[0])
        dates = dates.reverse()
        dates.forEach(date => {
            let today = new Date()
            let transactionDate = new Date(date)

            let months = today.getMonth() - transactionDate.getMonth() + (12 * (today.getFullYear() - transactionDate.getFullYear()));
            // console.log('months', months)
            if (today.getDate() < transactionDate.getDate()) {
                months--;
            }
            if (!monthsWithActivity.includes(months)) monthsWithActivity.push(months)
            return
        })

        let i = 1
        do {
            // console.log('mongs with a', monthsWithActivity)
            if (!monthsWithActivity.includes(i)) {
                i--
                break
            }
            i++
        }
        while (i <= 48)

        return i
    }

    getMonthsWithActivityScore(monthsWithActivity) {
        let score = 0
        if (monthsWithActivity < 12) score = 0
        if (monthsWithActivity >= 12 && monthsWithActivity < 24) score = 2
        if (monthsWithActivity >= 24 && monthsWithActivity < 47) score = 5
        if (monthsWithActivity >= 48) score = 10

        return {
            score,
            monthsWithActivity
        }
    }

    getAvgDailyActivityScore(numOfTransactions) {
        const dailyAvg = numOfTransactions / 365
        let score = 0
        if (dailyAvg == 0) score = 0
        if (dailyAvg > 0 && dailyAvg <= 1) score = 1
        if (dailyAvg > 1 && dailyAvg <= 2) score = 2
        if (dailyAvg > 2 && dailyAvg <= 5) score = 5
        if (dailyAvg > 5) score = 10

        return {
            score,
            dailyAvg
        }
    }
}

module.exports = EtherscanClient;