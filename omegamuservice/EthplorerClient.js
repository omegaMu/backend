// Added as hack to overcome TLS cert error 'Error: unable to verify the first certificate'  
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';

const axios = require('axios')

// const host = 'http://api.ethplorer.io/';
// const ethApiKey = 'freekey'
const headers = {
    'Content-Type': 'application/json',
    'rejectUnauthorized': false,
    'strictSSL': false
};

//   http://api.etherscan.io/api?module=account&action=
// txlist&address=0xddbd2b932c763ba5b1b7ae3b362eac3e8d40121a&
// startblock=0&endblock=99999999&sort=asc&apikey=YourApiKeyToken
// http://api.etherscan.io/api?module=account&action=txlist&address=0xde0b295669a9fd93d5f28d9ec85e40f4cb697bae&startblock=0&endblock=99999999&sort=asc&apikey=YourApiKeyToken
const apiPath = 'http://api.ethplorer.io/getAddressInfo/';
const apiTag = '?apiKey='
const apiKey = 'ijiw9573YUdH29'

// http://api.ethplorer.io/getAddressInfo/0xd716ad4f10eade5af4ed10006678f49bd2e6624f?apiKey=


class EthplorerClient {

    constructor() {
        this.exchangeRates = []
    }

    async getAccountScore(ethAddress, rates) {
        let tokensValue = 0
        this.exchangeRates = rates

        const account = await this.getAccount(ethAddress)

        // console.log('account', account)

        const balance = await this.calculateAccountBalance(account.ETH.balance)
        if (account.tokens) tokensValue = await this.getTokensValue(account.tokens)

        const totalBalance = balance + tokensValue
        console.log('accoutn blaance slcore', balance, tokensValue)

        const score = this.calculateScore(totalBalance)

        // console.log('balance score', score)
        return score || { score: 0, balance: 0 }
    }

    calculateScore(balance) {
        let score = 0

        if (balance > 100000) score = 5
        if (balance >= 10000 && balance < 100000) score = 2.5
        if (balance >= 1000 && balance < 10000) score = 1.5
        if (balance >= 1 && balance < 1000) score = .5
        if (balance < 1) score = 0

        return {
            score,
            balance
        }
    }

    getAccount(address) {
        return new Promise((resolve, reject) => {
            axios.get(`${apiPath}${address}${apiTag}${apiKey}`)
                .then(res => {
                    // console.log('res data', res.data)
                    resolve(res.data)
                })
                .catch(err => {
                    console.log('more error', err.response.data.error)
                })
        })
    }

    getTokensValue(tokens) {
        // console.log('tokens', tokens)

        let tokenPromises = []

        tokens.forEach(token => {
            tokenPromises.push(this.getTokenValue(token))
        })



        return Promise.all(tokenPromises).then(data => {
            return new Promise((resolve, reject) => {
                const totalDaiValue = data.length ? data.reduce((prev, next) => prev + next) : 0

                // console.log('total dai value', totalDaiValue)

                resolve(totalDaiValue)
            })
        })

    }

    getTokenValue(token) {
        // console.log('token', token.tokenInfo)
        const symbol = token.tokenInfo.symbol
        const decimals = token.tokenInfo.decimals
        // console.log('balance', balance / Math.pow(10, decimals))
        //translation error with wrapped eth and wrapped ether. 
        ///wrapped ether has value: 0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2
        //wrapped eth is worth 0 : 0xecf8f87f810ecf450940c9f60066b4a7a501d6a7
        //same symbol though, problem with the API. must remove anything with name 'Wrapped ETH'
        const balance = token.balance / Math.pow(10, decimals)

        return new Promise((resolve, reject) => {
            this.getConversionRate(symbol)
                .then(rate => {
                    // console.log('rate', rate, balance, symbol)
                    if (token.tokenInfo.name == 'Wrapped ETH') {
                        console.log(token)
                        resolve(0)
                    }
                    resolve(rate * balance)
                })
                .catch(err => {
                    resolve(0)
                    console.log('er at open debtr', err)
                })
        })
    }

    calculateAccountBalance(balance) {
        // console.log('running cal')
        return new Promise((resolve, reject) => {
            this.getConversionRate('ETH')
                .then(res => {
                    const balanceInDai = res * balance
                    // console.log('balance in dai', balanceInDai)
                    resolve(balanceInDai)
                })
                .catch(err => resolve(0))
        })
    }

    getConversionRate(symbol) {
        return new Promise((resolve, reject) => {
            // console.log()
            // if (symbol === 'WETH') console.log('this.exchangeRates', this.exchangeRates)
            if (this.exchangeRates[symbol]) resolve(this.exchangeRates[symbol])

            else {
                axios.get(`https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?symbol=${symbol}&convert=DAI`, {
                    headers: {
                        'X-CMC_PRO_API_KEY': '4febe3cf-b488-4dd5-af0c-685812021079'
                    }
                })
                    .then(res => {
                        if (symbol === 'WETH') console.log('weth', res.data.data[symbol])
                        resolve(res.data.data[symbol].quote.DAI.price)
                    })
                    .catch(err => {
                        resolve(0)
                        // console.log('conversion error in ethplorer', err.response.data, symbol)
                    })
            }
        })
    }
}

module.exports = EthplorerClient;