'use strict';
const OmegaMuService = require('./OmegaMuService');

const headers = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': true,
    'Content-Type': 'application/json',
}

module.exports.getScore = async (event, context, callback) => {
    const omegaMuService = new OmegaMuService();

    if (!event.pathParameters.id) {
        done({ message: "an id must be sent" }, null)
    }
    const ethAddress = event.pathParameters.id

    const done = (err, res) => {
        console.log('test here')
        // console.log(err, res, headers)
        //   callback(null, {
        //     isBase64Encoded: false,
        // statusCode: err ? 400 : 200,
        // headers,
        // body: err ? err.message : JSON.stringify(res),
        return {
            isBase64Encoded: false,
            statusCode: err ? 400 : 200,
            headers,
            body: err ? err.message : JSON.stringify(res)
        }
    }
    // )}

    switch (event.httpMethod) {

        case 'GET':

            //maybe this is the place to inspect the event obj to determine if we are generating a full report, or score only?

            //dynamo.scan({ TableName: event.queryStringParameters.TableName }, done);
            //const refresh = event.queryStringParameters.forceRefresh;

            // const ethAddress = '0x49739691fb5f3992b3f2536f309d955558e75933'

            //other addresses you can test. 
            // '0x9949bd0aefb3fa986ec52c439a99d88a6e9b77fb', delinquent
            // '0xdafdb75000fb0198c3cffc801a107c60f78298c2', del
            // '0xda9bf8e3e67882ba59e291b6897e3db114cf6bde', del
            // '0xf76fcbe0f3c39fe846d3c25cb5da7d73edb3e3c1', clear
            // '0x7f9b1225e33ee167ce59a3e9a303e9854c86d11e', del
            // '0x2c67eb139eb0b1416c3e743a106eb68d8442d627', del, total debt 1000 - 5000
            // '0xb3005003a2cd106007a886783dad0346958e9603', del
            // '0xc813a1d7a1781e3d021a77183d7aa6ff83c3f2bc', del
            // '0x00ba434edeccf437b6ccd423ce61391697fc0093', clear
            // '0x07fa740d495110810092c74bf4d03ddf975d7adc',  clear
            // '0xa56fd392cf1217449c75a8bcc4e3af732a0ba09e', clear
            // '0xbfb00c557ec40e9de7b0b60b3fbd9f2a95be1ee3', clear 
            // '0xbd319c59ca97f399f3b013675b40451b56d2543f', delinquent
            // '0x042ea089862d3631c225364c74f36cf055819015',  clear
            // '0x0c27b006629163b0dc728212e4ee39f51c973103' clear

            const score = await omegaMuService.getCreditScore(ethAddress, done, true)

            if (score.err) {
                console.log("??")
                return {
                    statusCode: 400,
                    headers,
                    body: JSON.stringify({ err: { message: 'That address not found, or an error occurred' } })
                }
            }

            //because using async await, using a callback brings up a serverless error. changing to return per https://github.com/dherault/serverless-offline/issues/434
            return {
                isBase64Encoded: false,
                statusCode: 200,
                headers,
                body: JSON.stringify({ score })
            }

            // omegaMuService.getLoanDetails(loanAddress)

            const dummyResponse = {
                score: 52.85,
                characteristics: {
                    difficultyFactor: {
                        value: .7
                    },
                    priorLoans: {
                        score: 40,
                        value: 0
                    },
                    openLoansBalance: {
                        score: 20,
                        value: 0
                    },
                    openLoansCredit: {
                        score: 0,
                        value: 0
                    },
                    lengthOfActivity: {
                        score: 2,
                        value: 12
                    },
                    overallActivity: {
                        score: 3,
                        value: 1.5
                    },
                    creditTypes: {
                        score: .5,
                        value: 1
                    },
                    totalAssetValue: {
                        score: 0,
                        value: 0
                    },
                    scoreUps: {
                        score: 10,
                        value: {
                            id: true,
                            stakedDai: 0,
                            coverage: 0
                        }
                    }
                }
            };
            // console.log('dummy!', done(null, dummyResponse ))
            // done(null, score );



            break;
        // default:
        //     done(new Error(`Unsupported method "${event.httpMethod}"`));

        //we will only handle GET methods for the time being.
        /*case 'DELETE':
            console.log('DELETE method invoked with body ' + event.body);
            break;
        case 'GET':
            console.log('GET method invoked with query string parameters ' + event.queryStringParameters);
            
            //could potenitally read the eth address form the query string.
            omegaMuService.exampleFunction('dummyEthAddress', done);
            break;
        case 'POST':
            console.log('POST method invoked with body ' + event.body);
            break;
        case 'PUT':
            console.log('PUT method invoked with body ' + event.body);
            break;
        */
    }
    // Use this code if you don't use the http event with the LAMBDA-PROXY integration
    // callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });


};
