process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';

const axios = require('axios')

const headers = {
    'Content-Type': 'application/json',
    'rejectUnauthorized': false,
    'strictSSL': false
}

class CMarkClient {

    async getRates() {
        //according to ethplorer api, all values are listed in eth
        return new Promise((resolve, reject) => {
            axios.get(`https://api.coinmarketcap.com/v2/ticker/?convert=dai`)
                .then(res => {
                    const rates = this.reduceRates(res.data.data)

                    resolve(rates)
                })
                .catch(err => {
                    console.log('conversion error', err)
                    resolve([0])

                })
        })
    }

    reduceRates(rawData) {
        let rateObject = {}
        Object.values(rawData).forEach(rate => {
            return rateObject[rate.symbol] = rate.quotes.DAI.price
        })

        return rateObject
    }
}

module.exports = CMarkClient;
