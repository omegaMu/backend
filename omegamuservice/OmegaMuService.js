const BloqBoardClient = require('./BloqboardClient.js');
const bbClient = new BloqBoardClient();

const EthplorerClient = require('./EthplorerClient.js');
const ethClient = new EthplorerClient();

const LoanscanClient = require('./LoanscanClient.js');
const loanClient = new LoanscanClient();

const EtherscanClient = require('./EtherscanClient.js');
const etherClient = new EtherscanClient();

const CMarkClient = require('./CMarkClient.js');
const cmarkClient = new CMarkClient();

const CreditMixClient = require('./CreditMixClient');
const creditMixClient = new CreditMixClient();

const doc = require('dynamodb-doc');
const dynamo = new doc.DynamoDB();

class OmegaMuService {

    async getCreditScore(ethAddress, callback, forceRefresh) {
        console.log('OmegaMuService.getCreditScore()...called');
        let totalScore = {}
        let score = 0

        const loans = await bbClient.getLoans(ethAddress)

        const creditMixScore = await creditMixClient.getCreditMix(ethAddress, loans.length)

        console.log('creditMixScore', creditMixScore)
        const exchangeRates = await cmarkClient.getRates()

        // console.log('exchange rates', exchangeRates)

        const loansDetailsScore = await loanClient.getLoansDetailsScore(loans, exchangeRates)
        console.log('loansdetails', loansDetailsScore)
        score += loansDetailsScore.score


        // console.log('tot 1', totalScore)

        const accountDetailsScore = await etherClient.getAccountTransactionDetailsScore(ethAddress, exchangeRates)
        score += accountDetailsScore.score

        // console.log('tot 3', totalScore)
        // ethClient.getAccountDetailsScore(ethAddress)

        const accountScore = await ethClient.getAccountScore(ethAddress, exchangeRates)

        score += accountScore.score
        // score += accountBalanceScore.score

        totalScore = {
            score,
            loansDetailsScore,
            accountDetailsScore,
            accountScore,
            creditMixScore
        }

        console.log('tot 2', totalScore)
        return totalScore
        console.log('total score?', totalScore)



        // bbClient.getLoans(ethAddress)
        //     .then(res => {
        //         return loanClient.getLoanDetailsScore(res)
        //     })
        //     .then(loanDetailsScore => {
        //         totalScore+=loanDetailsScore
        //             console.log('hopefuslly', loanDetailsScore)})

        //     .then(res => {
        //         console.log('get toal scredit score', totalScore)
        //     })
        // if(forceRefresh){

        //     //just make the first call to test connectivity
        //     bbClient.getLoans((result) => {
        //         console.log('colection of loans from bloqBoard: ' + result);
        //     });

        //     //TODO: implement real calculation
        //     console.log('OmegaMuService.getCreditScore()...returning calculated credit score ' + 'refreshedScore12345');
        //     callback(null, 'dummyRefreshedScore');
        // } else {
        //     const score = this.getCachedCreditScore(ethAddress);
        //     console.log('OmegaMuService.getCreditScore()...returning cached credit score ' + score);
        //     callback(null, score);
        // }
    }


    // the following functions are kinda like private, but not 
    // sure the JS construct for having private versus piblic functions
    // ----------------------------------------------------------------------------------

    calculateCreditScore(ethAddress) {

        bbClient.getLoans(() => {
            console.log('retrived a collection of loans, now find only the ones we need');

        });
    }

    getCachedCreditScore(ethAddress) {

        //TODO: implement dynamo call to get saved credit score
        return 1234577;
    }
}

module.exports = OmegaMuService;