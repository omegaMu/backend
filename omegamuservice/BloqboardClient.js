// Added as hack to overcome TLS cert error 'Error: unable to verify the first certificate'  
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
//const fetch = require("node-fetch");

const https = require('https');
const axios = require('axios')
// const host = 'loanscan.io';
// const apiPath = '/api';
// const baseLoansUrl = apiPath + '/loans';

const bbHost = 'https://api.bloqboard.com/'
const apiPath = 'api/v0/Debts?'
const debtorPath = 'debtorAddress='

const headers = {
    'Content-Type': 'application/json',
    'rejectUnauthorized': false,
    'strictSSL': false
};

class BloqBoardClient {

    async getLoans(ethAddress) {
        console.log('BloqBoardClient.getLoans() called', ethAddress);

        const loadRequestUrl = bbHost + apiPath + debtorPath + ethAddress;
        return await this.makeGetRequest(loadRequestUrl)
    }

    makeGetRequest(url) {
        return new Promise((resolve, reject) => {
            axios.get(url)
                .then(res => {
                    resolve(res.data.map(loan => {
                        //issuance hash is added when a loan is picked up (no longer available to a creditor)
                        if (loan.issuanceHash != null) return loan.issuanceHash
                    }))
                })
                .catch(err => {
                    console.log('err')
                    resolve(err)
                })
        })
    }
}

module.exports = BloqBoardClient;

// I wanted to use fetch, so I could return a promise and the use async/await.  
// But to do this I hand to bundle my node modules and deploy the lambda as a zip

/**
//     /  utility method for handling http status codes in a common
//     /  and manner for all 'fecth' calls
//     */
//    status(response) {
//         console.log('BloqBoardClient.status() - check the http response codes');
//         let returnPromise;
//         if (response.status >= 200 && response.status < 300) {
//             returnPromise = Promise.resolve(response);
//         } else {
//             console.log('Api.status() - response was something other than 2XX');
//             console.log(JSON.stringify(response));
//             returnPromise = Promise.reject(new Error(response.statusText));
//         }
//         return returnPromise;
//     }

//     /**
//     /  utility method for converting json response string to json
//     /  object in a common manner for all 'fetch' calls
//     */
//     json(response) {
//         console.log('BloqBoardClient.json() - Request completed with JSON response', JSON.stringify(response));
//         return response.json();
//     }

//     /**
//     /  common util method for handling boiler plate, secuirty, error checking,
//     /  and response derialization when posting to the CNAC api
//     */
//     getFromApi(url) {
//         console.log('fetching from ' + url);
//         return fetch(url, {
//             method: 'get',
//             headers: headers
//         })
//         .then(this.status)
//         .then(this.json)
//         .catch((error) => {
//             console.log('error making get request - ' + error);
//         });
//     }